import React from 'react'
import ReactDOM from 'react-dom'
import Family from './family'
import Member from './member'

ReactDOM.render(
    <Family  lastName='Junior'>
        <Member name='Onildo' />
    </Family>
    , document.getElementById('app')
)