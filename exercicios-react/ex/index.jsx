import React from 'react'
import ReactDOM from 'react-dom'
import Family from './family'
import Member from './member'

ReactDOM.render(
    <div>
        <Family  lastName='Junior'>
            <Member name='Onildo' />
            <Member name='João' />
            <Member name='Pedro' />
        </Family>
        <Family  lastName='Damasceno'>
            <Member name='Onildo' />
            <Member name='João' />
            <Member name='Pedro' />
        </Family>
    </div>
    , document.getElementById('app')
)